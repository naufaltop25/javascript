// Soal 1
// jawaban soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for(var i = 0; i < daftarHewan.length; i++)
{
    daftarHewan.sort();
    console.log(daftarHewan[i]);
}

// Soal 2
// jawaban soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
function introduce(tampilkan)
{
    return "Nama saya " + tampilkan.name + ", umur saya "  + tampilkan.age + " tahun, alamat saya di " + tampilkan.address + ", dan saya punya hobby yaitu " + tampilkan.hobby;
}
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
// jawaban soal 3
var hurufVokal = ["a", "e", "i", "o", "u"];
function hitung_huruf_vokal(str)
{
    var count = 0;

    for(var letter of str.toLowerCase())
    {
        if (hurufVokal.includes(letter)) 
        {
            count++;
        }
    }

    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2); // 3 2

// Soal 4
// jawaban soal 4
function hitung(angka)
{
    if(angka == 5)
    {
        angka = 8;
    } 
    else if(angka == 3)
    {
        angka = 4;
    } 
    else if(angka == 1)
    {
        angka = 0;
    }
    else if(angka == 0)
    {
        angka = -2;
    }
    return angka;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8


